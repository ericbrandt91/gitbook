# Einführung

Hallo und willkommen zur Dokumentation des SCIC HUP Systems des sächsichen *Chochlear Implant Centrum Dresden*!

Diese Dokumentation dient den Nutzern sowie den Entwicklern dazu, einen einfachen Einstieg in das SCIC HUP Software System zu erhalten.

Die Struktur dieses Dokumentes gliedert sich in die *Entwicklerdokumentation* und die *Nutzerdokumentation*

Die *Entwicklerdokumentation* liefert Antworten auf alle Fragen bezüglich des Programms und dessen Struktur.
Der *Quick Guide* bietet einen Überblick von üblichen Problemstellungen bei der Erweiterung und Pflege der Software.

Das Komplette Dokument ist ebenfalls als PDF verfügbar:
[PDF Dokumentation](./book.pdf)
Falls der Download dialog nicht öffnet, klicken Sie mit der rechten Maustaste auf den Link und wählen Sie "Ziel speichern unter..."