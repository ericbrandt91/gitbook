# Zusammenfassung

* [SCIC HUP](README.md)

* [Entwicklerdokumentation](./Entwicklerdokumentation/Einleitung.md)
    * [Architektur der Software](./Entwicklerdokumentation/Architektur.md)
    * [Prozessbeschreibung](./Entwicklerdokumentation/Prozesssicht.md)
    * [Komponentenbeschreibung](./Entwicklerdokumentation/Implementationssicht.md)
    	* [Datenbankservice](./Entwicklerdokumentation/Implementationssicht.md#datenbankservice)
    	* [Testhandler](./Entwicklerdokumentation/Implementationssicht.md#testhandler)
    	* [Regression](./Entwicklerdokumentation/Implementationssicht.md#regression)
    	* [AudioControl](./Entwicklerdokumentation/Implementationssicht.md#audiocontrol)
    	* [Kalibrierung](./Entwicklerdokumentation/Implementationssicht.md#kalibrierung)
    * [Logische Sicht](./Entwicklerdokumentation/Logischesicht.md)
    	* [Kalibrierung](./Entwicklerdokumentation/LogischKalibrierung.md#kalibrierung)
    	* [Models](./Entwicklerdokumentation/LogischModels.md#models)

	* [Quick Guide](./Entwicklerdokumentation/QuickGuide.md)
		* [Contribution GUIDE](./Entwicklerdokumentation/ContributionGuide.md)
		* [Erweiterung um ein Modul](./Entwicklerdokumentation/QuickGuide.md)

* [Nutzerdokumentation](./Nutzerdokumentation/Einleitung.md)
	* [Funktionalitäten](./Nutzerdokumentation/Ueberblick.md)  
	    * [Tonverwaltung](./Nutzerdokumentation/Ueberblick.md#tonverwaltung)    
	    * [Audiosetverwaltung](./Nutzerdokumentation/Ueberblick.md#audiosetverwaltung)  
	    * [Patienten](./Nutzerdokumentation/Ueberblick.md#patienten)  
