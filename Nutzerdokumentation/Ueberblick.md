# Überblick über die Grundfunktionalitäten

## Tonverwaltung
Die Tonverwaltung verwaltet alle Audiosequenzen, welche vom zuständigen Mitarbeiter in die Software eingepflegt werden. Hier ist es möglich neue Audiosequenzen anzulegen.

Navigieren Sie hierfür in die Tonverwaltung über das Hauptmenü. Sie sollten nun eine Anzeige entsprechend des folgenden Bildes erhalten.

![Tonverwaltung](./img/tonverwaltung.png)
### Sequenzen anzeigen

Im Tab Sequenzen werden alle Sequenzen angezeigt, die aktuell in das System eingepflegt wurden. 
Diese Ansicht ist die Standardansicht, wenn sie in diesen Menüpunkt navigieren.

### Sequenzen anlegen

Navigieren Sie in den Tab "Anlegen" wenn sie neue Sequenzen zu dem System hinzufügen möchten. Ziehen Sie dazu eine .wav Datei in das rechte Feld. Sie müssne nun einen Typ und eine Frequenz auswählen. Klicken Sie anschließend auf speichern, um die Daten in der Datenbank zu sichern.


## Audiosetverwaltung

Die Audiosetverwaltung dient dem Anlegen von Tests für die Hörüberprüfung. Sie gelangen über den Menüpunkt Audiosetverwaltung in das entsprechende Menü. Die Ansicht der Audiosetverwaltung sehen sie auf dem folgenden Bild.

![Audiosetverwaltung](./img/Testverwaltung.png)

### Audioset anlegen
Wollen Sie ein neues Testset anlegen, wählen sie einen Testton aus der rechten Liste aus und klicken Sie auf den Pfeil, der nach links zeigt. Die Liste der linken Seite sollte nun gefüllt sein.

Verändern Sie gegebenenfalls die Parameter wie die Dezibel und vergeben Sie einen Namen für das Testset.
Anschließend können Sie über den "Speichern" - Button das Testset sichern.

### Audioset löschen
Um ein Testset zu löschen wechseln Sie in den Tab Löschen und wählen Sie das entsprechende Testset aus.
Anschließend können Sie über einen klick auf "Testset löschen" das komplette Set entfernen.

## Patienten
Dieser Menüpunkt enthält alle Funktionalitäten um einen Test zu starten. Hier werden Patienten aufgelistet und ihm Tests zugewiesen.

Klicken Sie auf den Menüpunkt Patienten, um in die folgende Ansicht zu gelangen. 

![Patienten](./img/Patient_einrichten.png)

### Patienten auswählen und suchen
Wählen Sie anschließend einen Patienten aus der linken Liste aus. Neben dem Feld  "Aktueller Patient" sollte nun der von Ihnen gewählte Patient stehen. Tippen Sie den Namen in das Suchfeld um die Liste zu filtern, falls Sie Probleme haben sollten, einen Patienten zu finden. 

### Testset zuweisen

Wählen Sie aus der linken Liste einen Test aus (diese erscheint erst, wenn Sie einen Patienten ausgewählt haben). Das Feld "zugewiesener Test" sollte nun den von Ihnen ausgewählten Test anzeigen. 

### Zufällige Parameter

Falls sie die Reihenfolge und Dezibel zufällig in einem bestimmten Intervall festlegen wollen, können Sie die dies über einen Klick auf den Tab Zufälliger Test tun. Wählen sie einfach ein Intervall und geben sie dieses in die entsprechenden Felder ein. Klicken Sie anschließend auf Zufällige Pegel einstellen um das Testset neu zu verteilen.

