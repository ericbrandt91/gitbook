# Nutzerdokumentation
Die Nutzerdokumentation soll die Handhabung der Software vereinfachen und führt Schritt für Schritt durch Funktionalitäten des Programms.

Der erste Teil dieses Dokuments beinhaltet eine Auflistung aller Funktionen des Programms und deren Nutzung. Der zweite Teil beinhaltet kurze Anwenderspezifische Guides, die einen schnellen Einblick aus verschiedenen Perspektiven erläutern.

* [Überblick der Funktionen des SCIC HUP](./Ueberblick.html)
* [Anwender Quick Guides](./Anwender.html)

Die aktuelle Software befindet sich auf dem Stand der Version 1.3.2

![Mainscreen](./img/Mainscreen.PNG)