# Architektur
Die Architektur gliedert sich wie folgt:
  
> [Prozesssicht](Prozesssicht)   
Erhalte Beschreibungen über die Prozesse der Software und deren Beschreibung als Aktivitätsdiagramme. 
   
> [Implementationssicht](Implementationssicht)    
Erhalte Informationen der Komponenten und deren Beziehung untereinander. 
    
> [Logische Sicht](Logische Sicht)        
Erhalte konkrete Informationen einzelner Klassen und ihrer Funktion  
  
> [Physische Sicht](Physische Sicht)      
Erhalte Informationen über die externen Systeme und deren Integration (z.B. Datenbank, Soundkarte)  

