# Quick Guide
Der Quick Guide liefert schnelle Einführungen und Beispiele für übliche Use Cases
Wähle ein Thema, dass dich interessiert:

[Erweiterung des Hauptmenüs um ein weiteres Modul](#Erweiterung des Hauptmenüs um ein weiteres Modul)

# Erweiterung des Hauptmenüs um ein weiteres Modul

## 0. Aktueller Stand des Hauptmenüs
![Menustripe](./img/Menustripe.PNG)  
Neben dem Auswertungssymbol ganz rechts wollen wir eine weitere Schaltfläche unterbringen, welche uns ein neues Modul lädt.

## 1. Anlegen eines Testmoduls

Jedes Modul besteht aus folgenden Dingen:  
> View ( dies ist eine .xaml Datei)  
> ViewModel ( enthält die GUI Logik)  
> Model (enthält die Geschäftslogik)  
> ViewFactory( enthält das Wissen darüber wie das Modul geladen werden soll und welche Dienste übergeben werden)  

### 1.1 Die View
Navigiere in den Projektmappenexplorer und erstelle im Paket "UserControls" ein neues Benutzersteuerelement mit Namen TestAusgabe.xaml 
(Rechtsklick --> hinzufügen --> Benutzersteuerelement)  
Wechsle in das gerade erstellte Benutzersteuerelement und öffne die Datei mit der Endung xaml.cs. Füge dem Konstruktor eine Referenz auf das ViewModel hinzu (Wir erstellen dies im nächsten Schritt)  
Leite vom IView Interface ab.  


```cs
//TestAusgabe.xaml.cs
public partial class TestAusgabe : UserControl, IView
    {
        public TestAusgabe(VM_TestAusgabe vm)
        {
            InitializeComponent();
            DataContext = vm // Hier binden wir das ViewModel an die Oberfläche
        }
    }
```

Öffne Anschließend die Datei TestAusgabe.xaml und füge ein Label ein 
```html
<Label  Content="{Binding helloWorld}" HorizontalAlignment="Left" Margin="90,90,0,0" VerticalAlignment="Top"/>
```



### 1.2 Das ViewModel
Das ViewModel enthält die GUI Logik. Alle Veränderungen, die die GUI betreffen
Erstelle im Paket ViewModel eine neue Klasse (hinzufügen --> klasse) mit Namen VM_TestAusgabe und lass dies vom Typ ViewModelBase erben.

```cs
  public class VM_TestAusgabe : ViewModelBase
    {
        ModelTestAusgabe model;
        public string helloWorld { get { return model.fetchData(); } }


        public VM_TestAusgabe(ModelTestAusgabe model)
        {
            this.model = model;
        }
    }
```

### 1.3 Model
Das Model enthält die Geschäftslogik. Beispielsweise Datenzugriffe auf die Datenbank
Erstelle eine neue Klasse im Paket "Models". Dieses enthält eine Testmethode welche uns einen String zurück liefert.  

```cs
   public class ModelTestAusgabe
    {
        public string fetchData()
        {
            return "Neue Daten";
        }
    }
```

### 1.4 ViewFactory
Erstelle im Paket eine neue Klasse TestAusgabeFactory.
Die Methode CreateView instanziert ein neues Objekt mit allen benötigten Abhängikeiten. 

```cs
 class TestAusgabeFactory : IViewFactory
    {
        public bool AppliesTo(Type type)
        {
            return typeof(TestAusgabe).Equals(type);
        }

        public IView CreateView()
        {
            var model = new ModelTestAusgabe();
            var viewmodel = new VM_TestAusgabe(model);
            return new TestAusgabe(viewmodel);
        }
    }
```

## 2. Einbinden des neuen Moduls
Öffne die Datei MainWindow.xaml und füge eine weitere Spalte zu dem Grid hinzu.

```html
 <ColumnDefinition Width="1*"/>
...
...
<Controls:Tile  Grid.Column="6" BorderThickness="1" BorderBrush="Black" Height="Auto" Width="Auto" Cursor="Hand" Command="{Binding testausgabe}">
                <Grid Width="auto" >
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="2*"/>
                        <ColumnDefinition Width="1*"/>
                    </Grid.ColumnDefinitions>
                    <Viewbox Stretch="Uniform" Grid.Column="0">
                        <Rectangle Width="50" Height="50" Margin="0">
                            <Rectangle.Fill>
                                <VisualBrush Visual="{StaticResource appbar_zune}" />
                            </Rectangle.Fill>
                        </Rectangle>
                    </Viewbox>
                </Grid>
            </Controls:Tile>
```

Anschließend sollte das Menü wie folgt aussehen:  

![menu_new](./img/menu_new.PNG)  

## 2.1 Navigation im ViewModel hinzufügen

Öffne die Datei VM_MainWindow.cs und füge folgende Zeile hinzu
```cs
ICommand _TestAusgabe;
public ICommand testausgabe { get { return _TestAusgabe ?? (_TestAusgabe = new CommandHandler((e) => navigate(typeof(TestAusgabe)), true)); } }
```

# 3. Modul Im Bootstrapper hinzufügen
Öffne den Therapeuten Bootstrapper und füge folgende Zeile im Konstruktor hinzu

```cs
 viewFactory.addFactory(new TestAusgabeFactory());
```











