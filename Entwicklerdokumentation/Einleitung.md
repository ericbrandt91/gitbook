# Entwicklerdokumentation
Die Entwicklerdokumentation beinhaltet alle relevanten Beschreibungen der Software. Sowohl die innere als auch die äußere Sicht werden hier beleuchtet.

Verschaffe dir einen Überblick über folgende Themen: 

## Architektur
Erhalte alle nötigen Informationen bezüglich des Aufbaus der Software
* [Architektur](./Architektur.html)

## Quick Guides

> [Erweiterung des SCIC HUP um ein Modul](./QuickGuide.md)
