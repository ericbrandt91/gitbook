# Models
![Models](./img/Models.PNG)

Die Models enthalten die Geschäftslogik und haben Zugriff auf die Repositories 

# Code Example
```cs

//Übergabe aller benötigten Repository Interfaces
public MTonVerwaltung(ITonverwaltungRepository _repo, ITyp typRepo)
        {
           this._repo = _repo;
            this._typRepo = typRepo;
        }          
}
...
// Methoden die vom ViewModel aus aufgerufen werden enthalten die Geschäftslogik
public int SaveAudioSequenz(Audiosequenz sequenz,string path)
        {
            string source = path;
            string dest = sequenz.Pfad;
            try
            {
                File.Copy(source, dest);
            }
            catch (Exception)
            {

                return -1;
            }
                      
            return _repo.Save(sequenz);
        }


```





