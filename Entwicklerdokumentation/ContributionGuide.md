# Contribution Guide
Dieser Guide beinhaltet die Richtlinien für die Arbeit mit der Versionierung.

## 1. Klonen des Projektes
Lade dir das Projekt aus der Versionsverwaltung herunter.
* Starte dazu Visual Studio und öffne ein Quellcodeverwaltungsprojekt. (Datei -> Öffnen -> Aus Quellcodeverwaltung öffnen)
* Navigiere anschließend in den Team Explorer ( rechte Seite ) und klicke auf Verbindung verwalten ( grünes Steckersymbol )
* Wähle klonen und kopiere folgende URL in das Feld: https://ericbrandt91@gitlab.com/ericbrandt91/SCICAudioSW.git
* Klicke auf klonen und wähle das geklonte Projekt mit einem Doppelklick aus.
* Öffne anschließend die darin enthaltene Projektmappe.

## 2. Branches
Die Zweige dienen der Navigation innerhalb der Versionierung. 
Folgende Zweige sind für die Entwicklung vorhanden und relevant:
* master
* Developer

Der *master* Zweig enthält alle Produktivversionen der Software. Auf dem Developerzweig werden aktiv Veränderungen entwickelt und getestet. Alle Veröffentlichungen der Software werden von *master* Zweig aus getätigt.

## 3. Wechseln des Zweiges
Wähle innerhalb von Visual Studio Branches im Team Explorer und klicke dopelt auf den remote/origin Zweig Developer
![Git Branches](./img/git_branches)

Du befindest dich nun im Entwicklungszweig.

## 3. Entwicklung eines neuen Features
Ausgehend von deinem lokalen Entwicklerzweig(NICHT remote/origin), kannst du nun einen neuen Zweig erstellen, um dein neues Feature zu entwickeln. (Rechtsklick auf Developer -> Neuer lokaler Branch von)




