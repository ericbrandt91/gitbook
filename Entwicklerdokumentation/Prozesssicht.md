# Prozesssicht

Die Prozesssicht enthält alle UML Diagramme, welche der Beschreibung der Abläufe innerhalb des SCIC dienlich sind.

## Funktionalitäten
Die folgenden Funktionalitäten werden durch entsprechende UML Diagramme näher erläutert.

### Frequenztest durchführen
Beim Frequenztest werden dem Patienten Sinustöne verschiedener Frequenz abgespielt. Dieser bewertet anschließend diesen Test, welcher dann in der Datenbank gespeichert wird.

![Testdurchführung](./img/seq_testdurchf.PNG)

*Beschreibung*  

Der Therapeut registriert einen Patienten im System durch das Auswählen eines Patienten aus der Liste im Menüpunkt Patient. Anschließend weißt er ihm ein gewünschtes Testset hinzu und kann nun mit dem Test selbst beginnen.
Der Patient gibt je nach Test Bewertungen ab und der Therapeut speichert das Bewertungsset nach Beendigung des Tests.

*Zustandsdiagramm*  
![SystemKontext_3Ebene_Zustandsdiagramm_Testhandler](./img/SystemKontext_3Ebene_Zustandsdiagramm_Testhandler.PNG)

