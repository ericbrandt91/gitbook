# Komponentenbeschreibung aus Implementationssicht
Hier werden alle Komponenten beschrieben, die im SCIC HUP verwendet werden. 

## Komponenten

### Datenbankservice
Diese Komponenten kapselt den Datenbankzugriff. Sie enthält alle relevanten Abfragen auf die Datenbank.
Jedes Modul, welches einen Datenzugriff benötigt, muss das entsprechende Repository als Service erhalten.

Komponentendiagramm  
![Komponente_Diagram](./img/Komponente_Diagram.PNG) 

Beispiel der Nutzung: 


```cs
class MeinModul{
      IBewertungsRepository bewertungsAbfragen;
      //MeinModul erhält als Abhängigkeit die gewünschte Schnittstelle der Abfrage auf die Datenbank
      public MeinModul(IBewertungsRepository bewertungsAbfragen){
          this.bewertungsAbfragen = bewertungsAbfragen;
      }
      
      public List<Bewertung> ladeDaten(){         
           var list = bewertungsAbfragen.getAll()
           //do something with it
      }

}
```
### Testhandler
Der Testhandler wird überall da benötigt wo eine Überwachung des Zustands des aktuellen Tests nötig ist.
Komponentendiagram
![testhandler](./img/testhandler.PNG)

Dazu ist der Testhandler als shared Service an die Komponenten zu übergeben

```cs

//Während des Bootstrapping der Anwendung
//Einmalige instanzierung des Teshandlers
var testhandler = new TestHandler()
new ModuleA(testHandler);
new ModuleB(testHandler);
...
//----------------------
class ModuleA{
      ITesthandler testHandler;

      ModuleA(ITesthandler testHandler){
          this.testHandler = testHandler;
      }

      public verändereZustand(){
            testHandler.setTestState(200);
      }
}

class ModuleB{
      ITesthandler testHandler;

      ModuleA(ITesthandler testHandler){
          this.testHandler = testHandler;
      }

      public frageZustandAB(){
            
            Console.Writeline(testHandler.testState) //Ausgabe: 200
      }

}

```

### Regression

### AudioControl

### Kalibrierung

