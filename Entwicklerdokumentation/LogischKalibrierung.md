# Kalibrierung {#Kalibrierung}

![Kalibrierung](./img/Kalibrierung.png)

Der Kalibrierungshandler legt die Raumkalibrierung ( kalib ) über setSettings() fest und ruft diese über getSettings() wieder ab.

# Code Example
```cs
IKalibration kalib = new KalibrationHandler();
var databaseContext = new DatenbankKontext().Context;
var modelData = new RaumRaumsettingRepository().getModelDataForRoom(<roomid>);
var model = new Dictionary<int, LeastSquares>(); 

//Die benötigten Daten aus der Datenbank laden (Tabelle Kalibrierung)
//und lineare Regressionsmodelle erstellen
foreach(var tupel in modelData){
     double[] XArray = getXValuesFrom(modelData) //Dezibel pro Frequenz
     double[] YArray = getYValuesFrom(modelData) //relativen Dezibel pro Frequenz
     LeastSquares ls = new LeastSquares();
     ls.buildModel(XArray,YArray);
     
     model.Add(tupel, ls);     
          
}
kalib.setSettings(model);
```





